# Package LineImputer

This package allows to merge and impute files. It contains three functions: `mergevcf`, `imputation` and `impThreshold`.    The function `mergevcf` is designed for merging two or more, VCF files but it can be applied to other highly structured file types.    The function `imputation` allows to predict the unknown values in VCF files. As distinct from the function `mergevcf` this function can be applied only to VCF files.    The function `impThreshold` allows to process the files after imputation. This function can be applied only to VCF files.

# Download

Here, with this links you can access the latest package and the check
log build by the Continuous Integration:

 - [latest]
 - [1.0.2][v1.0.2]
 - [1.0.1][v1.0.1]



[latest]: https://forgemia.inra.fr/tristan.mary-huard/LineImputer/builds/artifacts/master/browse?job=package
[v1.0.1]: https://forgemia.inra.fr/tristan.mary-huard/LineImputer/builds/artifacts/v1.0.1/browse?job=package
[v1.0.2]: https://forgemia.inra.fr/tristan.mary-huard/LineImputer/builds/artifacts/v1.0.2/browse?job=package
