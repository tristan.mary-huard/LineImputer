//
//  ToBinaryTransformer.cpp
//  Impute
//
//  Created by Michel Koskas on 02/10/2016.
//  Copyright © 2016 INRA. All rights reserved.
//

#include "ToBinaryTransformer.h"
#include <assert.h>
#include <cstdlib>
#include <fstream>
#include <iostream>

// Cette fonction "traiter une ligne" extrait le genotypage d'une ligne (d'un
// SNP) et ecrit dans un tableau la convertion en 0,1,. a partir de 0|0, 1|1,
// .|. ou .|0 ou 0|. ou .|1 ou 1|. Rq: 0 represente l'allele de reference (du
// genome B73) et 1 l'allele alternatif
void ToBinaryTransformer::TreatOneLine(char *BeginLine, char *EndLine,
                                       std::ofstream &OutputFile, int &NbPoints,
                                       int &TotalDataSize, unsigned int &NbInd,
                                       unsigned int &NbMarkers) {
    bool FirstLine = (NbInd == 0);
    if (BeginLine[0] == '#')
        return;
    bool Written = false;
    for (int i = 1; i < EndLine - BeginLine; i++) {
        if ((BeginLine[i] == '|') || (BeginLine[i] == '/')) {
            Written = true;
            char c;
            if ((BeginLine[i - 1] == '.') || (BeginLine[i + 1] == '.'))
                c = '2';
            else if (BeginLine[i - 1] != BeginLine[i + 1])
                c = '2';
            else if (BeginLine[i - 1] ==
                     '0') // 0 est l'allele de reference (du genome B73)
                c = '0';
            else
                c = '1'; // 1 est l'allele alternatif
            if (c == '2')
                NbPoints++;
            TotalDataSize++;
            OutputFile << c;
            if (FirstLine)
                NbInd++;
        }
    }
    if (Written) {
        OutputFile << '\n';
        NbMarkers++;
    }
}

// La fonction "traiter un buffer" permet de traiter des fenetres le long du
// fichier vcf
int ToBinaryTransformer::TreatBuffer(char *Buffer, int BufferSize,
                                     std::ofstream &OutputFile, int &NbPoints,
                                     int &TotalDataSize, unsigned int &NbInd,
                                     unsigned int &NbMarkers) {
    // At each moment BeginLine is the biggining of the current line.
    // EndLine should contain the last index of the current line.
    // At first Endline is BeginLine
    int BeginLine = 0, EndLine = BeginLine;

    while (EndLine < BufferSize) {
        while ((EndLine < BufferSize) && (Buffer[EndLine] != '\n'))
            EndLine++;
        // If Endline is '\n', Buffer contains a complete line to be treated.
        // Otherwise (namely if Endline == BufferSize) the line is incomplete.
        if (EndLine == BufferSize)
            break; // break is ok because the line is incomplete
        // Now we know we have a complete line...
        TreatOneLine(Buffer + BeginLine, Buffer + EndLine, OutputFile, NbPoints,
                     TotalDataSize, NbInd, NbMarkers);
        BeginLine = EndLine + 1;
        EndLine = BeginLine;
    }
    return BeginLine;
}

ToBinaryTransformer::ToBinaryTransformer() {
    InputFileName = "";
    OutputFileName = "";
}

ToBinaryTransformer::ToBinaryTransformer(std::string I, std::string O,
                                         Imputator *Im, unsigned int &NbInd,
                                         unsigned int &NbMarkers) {
    MyImputator = Im;
    InputFileName = I;
    OutputFileName = O;
    CreateFile(NbInd, NbMarkers);
}

//
void ToBinaryTransformer::CreateFile(unsigned int &NbInd,
                                     unsigned int &NbMarkers) {
    std::ifstream InputFile(InputFileName.c_str(),
                            std::ifstream::in | std::ifstream::binary);
    std::ofstream OutputFile(OutputFileName.c_str(),
                             std::ofstream::out | std::ofstream::binary);

    NbInd = 0;
    NbMarkers = 0;
    unsigned long int FileSize;
    InputFile.seekg(0, std::ios::end);
    FileSize = InputFile.tellg();
    unsigned long int ReadChars = 0;
    InputFile.seekg(0, std::ios::beg);

    int BufferSize = 50000000, Residu = 0;
    char *Buffer = new char[BufferSize];
    int NbPoints = 0, TotalDataSize = 0;
    while (ReadChars < FileSize) {
        InputFile.read(Buffer + Residu, BufferSize - Residu);
        unsigned int JustReadChars = (unsigned int)InputFile.gcount();
        ReadChars += JustReadChars;
        int BufferPosition =
            TreatBuffer(Buffer, Residu + JustReadChars, OutputFile, NbPoints,
                        TotalDataSize, NbInd, NbMarkers);
        for (int i = BufferPosition; i < BufferSize; i++)
            Buffer[i - BufferPosition] = Buffer[i];
        Residu = BufferSize - BufferPosition;
        assert(Residu >= 0);
    }
    delete[] Buffer;
    Buffer = NULL;
}
