#ifndef TRIPLE_H
#define TRIPLE_H

#include <string>

class Triple {
    // Fields
  private:
    std::string Name;
    int Index_1;
    int Index_2;

  public:
    // Constructeurs
  private:
  public:
    void Initialize(std::string S, int I, int J);
    Triple();
    Triple(std::string S, int I, int J);
    bool operator<(const Triple &Other);
    std::string GetName() const;
    int GetIndex_1() const;
    int GetIndex_2() const;
};

// std::ostream & operator<<(std::ostream &s, const Triple &T);

#endif // TRIPLE_H
