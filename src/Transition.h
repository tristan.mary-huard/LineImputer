// File Transition.h

#ifndef Transition_h
#define Transition_h

#include "Constants.h"
#include "GeneralFunctions_imp.h"
#include "Imputator.h"
#include <iostream>

class Imputator;

class Transition {
    ////////////
    // Fields //
    ////////////
  public:
    Imputator *MyImputator;
    double **MyTransitions;
    unsigned int MyIndex;

  private:
    unsigned short int MyMarkovChainOrder;

    /////////////
    // Methods //
    /////////////

  public:
    Transition();
    Transition(Imputator *I, unsigned int Index, char **Data,
               unsigned short int MarkovChainOrder);
    void Initialize(Imputator *I, unsigned int Index, char **Data,
                    unsigned short int MarkovChainOrder);
    long double Proba(unsigned int Antecedent, int NextDigit);
    ~Transition();
};

std::ostream &operator<<(std::ostream &s, const Transition &T);

// Calcul les probas de transition

#endif
