#include "Transition.h"
#include "Imputator.h"
#include <assert.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>

Transition::Transition() {
    MyImputator = NULL;
    MyIndex = -1;
    MyTransitions = NULL;
}

Transition::Transition(Imputator *I, unsigned int Index, char **Data,
                       unsigned short int MarkovChainOrder) {
    Initialize(I, Index, Data, MarkovChainOrder);
}

// char **Data is the data to be imputed.
// It contains exactly MARKOVCHAINORDER + 1 lines.
// Each line concerns a given locus.

// TODO Seriously check next function
void Transition::Initialize(Imputator *I, unsigned int Index, char **Data,
                            unsigned short int MarkovChainOrder) {
    MyMarkovChainOrder = MarkovChainOrder;
    MyImputator = I;
    MyIndex = Index;
    double InitialCount = MyImputator->InitialCount;
    MyTransitions = new double *[(1 << MarkovChainOrder)];
    for (unsigned int i = 0; i < (unsigned int)(1 << MarkovChainOrder); i++)
        MyTransitions[i] = new double[2];
    for (unsigned int i = 0; i < (unsigned int)(1 << MarkovChainOrder); i++)
        for (int j = 0; j < 2; j++)
            MyTransitions[i][j] = InitialCount;

    unsigned int NbInd = MyImputator->GetNbInd();
    unsigned int Nb2 = 0;
    unsigned int n = 0;
    unsigned char PositionsOf2[MarkovChainOrder + 1];
    char TransverseData[MarkovChainOrder + 1];
    for (unsigned int Ind = 0; Ind < NbInd; Ind++) {
        for (int i = 0; i < MarkovChainOrder + 1; i++)
            TransverseData[i] = Data[i][Ind];

        n = GetInitial(TransverseData, PositionsOf2, Nb2, MarkovChainOrder + 1);
        if (Nb2 == (unsigned int)(MarkovChainOrder + 1))
            continue;
        if (Nb2 == MarkovChainOrder)
            if ((TransverseData[0] != '2') ||
                (TransverseData[MarkovChainOrder] != '2'))
                continue;
        // Now we know for sure that both antecedant and image have at least a
        // known char.
        double Quantum = 1. / (1 << Nb2);
        for (int i = 0; i < (1 << Nb2); i++) {
            // Fill renvoie un entier dans lequel les chiffres 2 ont été
            // remplacés par ceux de l'écriture binaire de i. Cet entier est
            // écrit sur MARKOVCHAINORDER + 1 chiffres binaires. Les
            // MARKOVCHAINORDER chiffres de poids faible forment l'antécédant
            // Les MARKOVCHAINORDER chiffres de poids fort forment l'image.
            unsigned int ConsecutiveLetters = Fill(n, PositionsOf2, i, Nb2);
            unsigned int Antecedent =
                ConsecutiveLetters % (1 << (MarkovChainOrder));
            // unsigned int MappedLetter = ConsecutiveLetters / 2;
            unsigned int LastDigit = ConsecutiveLetters >> MarkovChainOrder;
            MyTransitions[Antecedent][LastDigit] += Quantum;
        }
    }
}

Transition::~Transition() {
    for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
        delete[] MyTransitions[i];
    delete[] MyTransitions;
}

long double Transition::Proba(unsigned int Antecedent, int NextDigit) {
    assert(Antecedent < (unsigned int)(1 << MyMarkovChainOrder));
    assert(NextDigit >= 0);
    assert(NextDigit < 2);
    long double Numerateur =
        MyTransitions[Antecedent][NextDigit] + MyImputator->InitialCount;
    long double Denominateur = MyTransitions[Antecedent][0] +
                               MyTransitions[Antecedent][1] +
                               2 * MyImputator->InitialCount;
    return Numerateur / Denominateur;
}
