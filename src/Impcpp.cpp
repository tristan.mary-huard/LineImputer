// // [[Rcpp::plugins(cpp11)]]
// #include <Rcpp.h>
// using namespace Rcpp;
// #include <iostream>
// #include <fstream>
// #include <assert.h>
// #include <string>
// #include <sstream>
// #include <cstdlib>
// #include "Imputator.h"
// #include "GeneralFunctions_imp.h"
// #include "Transition.h"
//
// // [[Rcpp::export]]
// void ImputatorCpp(NumericVector markov_order,
//                   CharacterVector input_path,
//                   CharacterVector input_filename,
//                   CharacterVector output_path,
//                   NumericVector threshold,
//                   NumericVector initial_count,
//                   LogicalVector do_viterbi,
//                   LogicalVector do_probas) {
//   unsigned short int f0 = Rcpp::as<unsigned short int>(markov_order);
//   std::string f1 = Rcpp::as<std::string>(input_path);
//   std::string f2 = Rcpp::as<std::string>(input_filename);
//   std::string f3 = Rcpp::as<std::string>(output_path);
//   long double f4 = Rcpp::as<long double>(threshold);
//   long double f5 = Rcpp::as<long double>(initial_count);
//   bool f6 = Rcpp::as<bool>(do_viterbi);
//   bool f7 = Rcpp::as<bool>(do_probas);
//   Imputator(f0, f1, f2, f3, f4, f5, f6, f7);
// }
