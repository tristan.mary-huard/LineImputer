#include "couple.h"

Couple::Couple() {
    Name = "";
    Index = -1;
}

Couple::Couple(std::string S, int I) { Initialize(S, I); }

void Couple::Initialize(std::string S, int I) {
    Name = S;
    Index = I;
}

bool Couple::operator<(const Couple &Other) {
    return namecomparator(GetName(), Other.GetName());
}

std::string Couple::GetName() const { return Name; }

int Couple::GetIndex() const { return Index; }

bool namecomparator(const std::string &first, const std::string &second) {
    long int first_intname = atol(first.c_str());
    long int second_intname = atol(second.c_str());
    if (first_intname < second_intname)
        return true;
    if (first_intname > second_intname)
        return false;
    return first < second;
}
