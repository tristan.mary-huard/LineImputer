//
//  Proba.hpp
//  Sandra
//
//  Created by Michel Koskas on 04/07/2016.
//  Copyright © 2016 INRA. All rights reserved.
//

#ifndef MarkovChain_hpp
#define MarkovChain_hpp

#include "Transition.h"
#include <fstream>
#include <stdio.h>
#include <string>

class Transition;

// This class computes, stores and restores the Markov Chains.
class Imputator {
    ////////////
    // Fields //
    ////////////

  public:
    bool DoViterbi;
    bool DoProbas;
    long double Threshold;
    long double InitialCount;
    unsigned short int MyMarkovChainOrder;

  private:
    unsigned int NbInd;
    unsigned int NbMarkers;
    long double *Forward;
    long double *G;
    long double *Tau;
    std::string InputFileName;
    std::string InputFileNameWithConstants;
    std::string InputPath;
    std::string OutputPath;
    std::string InitialMatrixFileName;
    std::string TransposedMatrixFileName;
    std::string ForwardBackwardImputedFileName;
    Transition *MyTransitions;
    std::ofstream CheckTausFile;
    std::ofstream ViterbiFile;
    std::ofstream ProbasCharsFile;
    std::ofstream ProbasBinaryFile;
    std::ofstream ProbasHRNumbersFile;
    bool CheckTauCoherence;
    std::string ViterbiFileName;
    std::string NumbersProbasFileName;
    std::string BinaryProbasFileName;
    std::string CharsProbasFileName;
    unsigned char *PositionsOf2;

    /////////////
    // Methods //
    /////////////

  public:
    Imputator();
    Imputator(unsigned short int MarkCO, std::string InputP,
              std::string InputFN, std::string OutputP,
              long double Thresh = 0.75, long double InitialC = 0.5,
              bool DV = true, bool DP = true, bool CheckTaus = false);
    unsigned int GetNbMarkers();
    unsigned int GetNbInd();

  private:
    void Init();
    void MoveForwardBackward();
    void MoveForward(char *Ind);
    void MoveBackward(char *Ind);
    void Viterbi(char *Ind);
    void WriteProbasImputation(char *Ind);
    long double &GetTau(unsigned int Locus, unsigned int Letter);
    long double &GetG(unsigned int Locus, unsigned int Letter);
    long double &GetForward(unsigned int Locus, unsigned int Letter);
    void CheckTaus();
    long double ProbaToBe1FixedDigit(unsigned int Locus,
                                     unsigned char DistanceToLocus);
    void InitializeInitialProbabilities(char *Ind);
};

std::ostream &operator<<(std::ostream &s, const Imputator &I);

#endif
