#include "triple.h"

Triple::Triple() {
    Name = "";
    Index_1 = -1;
    Index_2 = -1;
}

Triple::Triple(std::string S, int I, int J) { Initialize(S, I, J); }

void Triple::Initialize(std::string S, int I, int J) {
    Name = S;
    Index_1 = I;
    Index_2 = J;
}

bool Triple::operator<(const Triple &Other) {
    return GetName() < Other.GetName();
}

std::string Triple::GetName() const { return Name; }

int Triple::GetIndex_1() const { return Index_1; }

int Triple::GetIndex_2() const { return Index_2; }
