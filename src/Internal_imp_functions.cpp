// [[Rcpp::plugins(cpp11)]]
#include <Rcpp.h>
using namespace Rcpp;
#include "GeneralFunctions_imp.h"
#include "Imputator.h"
#include "ToBinaryTransformer.h"
#include "Transition.h"
#include <assert.h>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

// [[Rcpp::export]]
void ImputatorCpp(NumericVector markov_order, CharacterVector input_path,
                  CharacterVector input_filename, CharacterVector output_path,
                  NumericVector threshold, NumericVector initial_count,
                  LogicalVector do_viterbi, LogicalVector do_probas) {
    unsigned short int f0 = Rcpp::as<unsigned short int>(markov_order);
    std::string f1 = Rcpp::as<std::string>(input_path);
    std::string f2 = Rcpp::as<std::string>(input_filename);
    std::string f3 = Rcpp::as<std::string>(output_path);
    long double f4 = Rcpp::as<long double>(threshold);
    long double f5 = Rcpp::as<long double>(initial_count);
    bool f6 = Rcpp::as<bool>(do_viterbi);
    bool f7 = Rcpp::as<bool>(do_probas);
    Imputator(f0, f1, f2, f3, f4, f5, f6, f7);
}

// [[Rcpp::export]]
void ImpThreshold_cpp(CharacterVector probas_file, CharacterVector output_file,
                      NumericVector threshold, NumericVector nb_supp_elements,
                      NumericVector nb_cols, LogicalVector replace_values,
                      LogicalVector do_threshold) {
    std::string f1 = Rcpp::as<std::string>(probas_file);
    std::string f2 = Rcpp::as<std::string>(output_file);
    double f3 = Rcpp::as<double>(threshold);
    int f4 = Rcpp::as<int>(nb_supp_elements);
    int f5 = Rcpp::as<int>(nb_cols);
    bool f6 = Rcpp::as<bool>(replace_values);
    bool f7 = Rcpp::as<bool>(do_threshold);

    ImpThreshold(f1, f2, f3, f4, f5, f6, f7);
}

// [[Rcpp::export]]
void txttovcfCpp(CharacterVector txt_filename, CharacterVector vcf_filename,
                 CharacterVector output_filename,
                 NumericVector nb_supp_elements, NumericVector nb_cols,
                 LogicalVector replace_values) {
    std::string f1 = Rcpp::as<std::string>(txt_filename);
    std::string f2 = Rcpp::as<std::string>(vcf_filename);
    std::string f3 = Rcpp::as<std::string>(output_filename);
    int f4 = Rcpp::as<int>(nb_supp_elements);
    int f5 = Rcpp::as<int>(nb_cols);
    bool f6 = Rcpp::as<bool>(replace_values);

    txt_to_vcf(f1, f2, f3, f4, f5, f6);
}
