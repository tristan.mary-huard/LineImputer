#ifndef COUPLE_H
#define COUPLE_H

#include <stdlib.h>
#include <string>

class Couple {
    // Fields
  private:
    std::string Name;
    int Index;

  public:
    // Methods
  private:
  public:
    Couple();
    Couple(std::string S, int I);
    bool operator<(const Couple &Other);
    std::string GetName() const;
    int GetIndex() const;
    void Initialize(std::string S, int I);
};

bool namecomparator(const std::string &first, const std::string &second);

#endif // COUPLE_H
