//
//  MarkovChain.cpp
//  Sandra
//
//  Created by Michel Koskas on 04/07/2016.
//  Copyright © 2016 INRA. All rights reserved.
//

#include "Imputator.h"
#include "GeneralFunctions_imp.h"
#include "Transition.h"
#include <assert.h>
#include <cstdio>
#include <cstdlib>
#include <sstream>

/////////////                      Constructors and Destrcutors /////////////

Imputator::Imputator() {
    Forward = NULL;
    G = NULL;
    Tau = NULL;
    InputPath = "";
    OutputPath = "";
    InputFileName = "";
    InputFileNameWithConstants = "";
    DoViterbi = true;
    DoProbas = true;
}

Imputator::Imputator(unsigned short int MarkovCO, std::string InputP,
                     std::string InputFN, std::string OutputP,
                     long double Thresh, long double InitialC, bool DV, bool DP,
                     bool CheckTaus) {
    Threshold = Thresh;
    InitialCount = InitialC;
    InputPath = InputP;
    OutputPath = OutputP;
    InputFileName = InputFN;
    CheckTauCoherence = CheckTaus;
    DoViterbi = DV;
    DoProbas = DP;
    MyMarkovChainOrder = MarkovCO;
    std::ostringstream Os;
    Os << MyMarkovChainOrder;
    InputFileNameWithConstants = InputFileName + "_MarkOrd_" + Os.str();
    Os.str("");
    Os << Threshold;
    InputFileNameWithConstants =
        InputFileNameWithConstants + "_Thres_" + Os.str();
    Os.str("");
    Os << InitialCount;
    InputFileNameWithConstants =
        InputFileNameWithConstants + "_InitialProb_" + Os.str();
    PositionsOf2 = new unsigned char[MyMarkovChainOrder];
    Init();
}

/////////////                      General purpose functions /////////////

unsigned int Imputator::GetNbInd() { return NbInd; }

unsigned int Imputator::GetNbMarkers() { return NbMarkers; }

long double &Imputator::GetTau(unsigned int Locus, unsigned int Letter) {
    assert(Letter < (unsigned int)(1 << MyMarkovChainOrder));
    assert(Letter >= 0);
    assert(Locus >= 0);
    assert(Locus < NbMarkers);
    return Tau[Locus * (1 << MyMarkovChainOrder) + Letter];
}

long double &Imputator::GetG(unsigned int Locus, unsigned int Letter) {
    assert(Letter < (unsigned int)(1 << MyMarkovChainOrder));
    assert(Letter >= 0);
    assert(Locus >= 0);
    assert(Locus < NbMarkers);
    return G[Locus * (1 << MyMarkovChainOrder) + Letter];
}

long double &Imputator::GetForward(unsigned int Locus, unsigned int Letter) {
    assert(Letter < (unsigned int)(1 << MyMarkovChainOrder));
    assert(Letter >= 0);
    assert(Locus >= 0);
    assert(Locus < NbMarkers);
    return Forward[Locus * (1 << MyMarkovChainOrder) + Letter];
}

long double Imputator::ProbaToBe1FixedDigit(unsigned int Locus,
                                            unsigned char DistanceToLocus) {
    long double Answer = 0;
    unsigned int RealLocus = Locus - DistanceToLocus;
    for (unsigned int LetterIndex = 0;
         LetterIndex < (unsigned int)(1 << (MyMarkovChainOrder - 1));
         LetterIndex++) {
        unsigned int Quotient = LetterIndex / (1 << DistanceToLocus);
        unsigned int Remaining = LetterIndex % (1 << DistanceToLocus);
        unsigned int RealLetterIndex = Quotient * (1 << (DistanceToLocus + 1)) +
                                       (1 << DistanceToLocus) + Remaining;
        Answer += GetTau(RealLocus, RealLetterIndex);
    }
    return Answer;
}

/////////////                      Imputation functions /////////////

////      General initialization of the Imputator

// The same arrays of numbers are used for eaach single Ind who is imputed.
// These arrays need to be re-initialized prior to any treatment.
// This function should hence be called before any imputation wether it were
// Viterbi or Proba.
void Imputator::InitializeInitialProbabilities(char *Ind) {
    unsigned int MaxLetter = (1 << MyMarkovChainOrder);
    for (unsigned int Locus = 0; Locus < NbMarkers - MyMarkovChainOrder + 1;
         Locus++)
        for (unsigned int Letter = 0; Letter < MaxLetter; Letter++) {
            GetForward(Locus, Letter) = 0;
            GetG(Locus, Letter) = 0;
            GetTau(Locus, Letter) = 0;
        }
    // unsigned char PositionsOf2[MARKOVCHAINORDER];
    unsigned int Nb2 = 0;
    unsigned int n = 0;
    // Initializing Forward array
    n = GetInitial(Ind, PositionsOf2, Nb2, MyMarkovChainOrder);
    for (int i = 0; i < (1 << Nb2); i++) {
        unsigned int Letter = Fill(n, PositionsOf2, i, Nb2);
        GetForward(0, Letter) = ((long double)1) / (1 << Nb2);
    }
}

void Imputator::Init() {
    // Step 1:
    // Create InitialMatrixFile
    // Compute NbInd and NbMarkers

    std::string FullInputFileName = InputPath + InputFileName + ".vcf";

    std::string FullOutputFileName =
        OutputPath + InputFileNameWithConstants + "_InitialMatrix.txt";
    FromVCFToMatrix(FullInputFileName, FullOutputFileName, NbInd, NbMarkers);

    // Step 2:
    // Allocate an Initialize the Transition matrices
    MyTransitions = new Transition[NbMarkers - MyMarkovChainOrder + 1];
    char **M = new char *[MyMarkovChainOrder + 1];
    for (int i = 0; i < MyMarkovChainOrder + 1; i++)
        M[i] = new char[NbInd + 1];
    FullInputFileName =
        OutputPath + InputFileNameWithConstants + "_InitialMatrix.txt";
    std::ifstream InputInitialMatrixFile(
        FullInputFileName.c_str(), std::ifstream::in | std::ifstream::binary);
    assert(InputInitialMatrixFile.is_open());
    for (int i = 0; i < MyMarkovChainOrder; i++)
        InputInitialMatrixFile.read(M[i], NbInd + 1);
    for (unsigned int i = 0; i < NbMarkers - MyMarkovChainOrder; i++) {
        InputInitialMatrixFile.read(M[MyMarkovChainOrder], NbInd + 1);
        MyTransitions[i].Initialize(this, i, M, MyMarkovChainOrder);
        for (int NumMarker = 0; NumMarker < MyMarkovChainOrder; NumMarker++)
            for (unsigned int NumInd = 0; NumInd < NbInd; NumInd++)
                M[NumMarker][NumInd] = M[NumMarker + 1][NumInd];
    }
    for (int i = 0; i < MyMarkovChainOrder + 1; i++)
        delete[] M[i];
    delete[] M;

    // Step 3
    // Transpose the initial matrix

    std::string TranspodedMatrixFileName;
    FullInputFileName =
        OutputPath + InputFileNameWithConstants + "_InitialMatrix.txt";
    TranspodedMatrixFileName =
        OutputPath + InputFileNameWithConstants + "_TransposedMatrix.txt";
    Transpose(FullInputFileName, TranspodedMatrixFileName);

    // Step4:
    // Compute and store the imputed data.
    if (CheckTauCoherence) {
        std::string CheckFileName =
            OutputPath + InputFileNameWithConstants + "_CheckTaus.txt";
        CheckTausFile.open(CheckFileName.c_str(),
                           std::ofstream::out | std::ofstream::binary);
        assert(CheckTausFile.is_open());
    }
    if (DoViterbi) {
        ViterbiFileName = OutputPath + InputFileNameWithConstants +
                          "_ImputedData_Viterbi.txt";
        ViterbiFile.open(ViterbiFileName.c_str(),
                         std::ofstream::out | std::ofstream::binary);
        assert(ViterbiFile.is_open());
    }

    MoveForwardBackward();
    delete[] PositionsOf2;
}

void Imputator::CheckTaus() {
    for (unsigned int Locus = 0; Locus < NbMarkers; Locus++) {
        CheckTausFile << "Locus = " << Locus << " : ";
        for (unsigned int DistanceToLocus = 0;
             DistanceToLocus < MyMarkovChainOrder; DistanceToLocus++) {
            if (Locus < DistanceToLocus)
                continue;
            long double P1 = ProbaToBe1FixedDigit(Locus, DistanceToLocus);
            CheckTausFile << P1 << '\t';
        }
        CheckTausFile << '\n';
    }
}

void Imputator::MoveForwardBackward() {

    std::string FileName =
        OutputPath + InputFileNameWithConstants + "_TransposedMatrix.txt";
    std::ifstream InputFile(FileName.c_str(),
                            std::ifstream::in | std::ifstream::binary);
    assert(InputFile.is_open());

    if (DoProbas) {
        NumbersProbasFileName = OutputPath + InputFileNameWithConstants +
                                "_ImputedData_Probabilities.txt";
        ProbasHRNumbersFile.open(NumbersProbasFileName.c_str(),
                                 std::ofstream::out | std::ofstream::binary);
        assert(ProbasHRNumbersFile.is_open());

        BinaryProbasFileName = OutputPath + InputFileNameWithConstants +
                               "_ImputedData_ProbasFile_BinaryFormat.bin";
        ProbasBinaryFile.open(BinaryProbasFileName.c_str(),
                              std::ofstream::out | std::ofstream::binary);
        assert(ProbasBinaryFile.is_open());

        CharsProbasFileName = OutputPath + InputFileNameWithConstants +
                              "_ImputedData_ForwardBackward.txt";
        ProbasCharsFile.open(CharsProbasFileName.c_str(),
                             std::ofstream::out | std::ofstream::binary);
        assert(ProbasCharsFile.is_open());
    }

    char *Ind = new char[NbMarkers + 1];
    unsigned int MaxLetter = (1 << MyMarkovChainOrder);
    Forward = new long double[MaxLetter * (NbMarkers - MyMarkovChainOrder + 1)];
    G = new long double[MaxLetter * (NbMarkers - MyMarkovChainOrder + 1)];
    Tau = new long double[MaxLetter * (NbMarkers - MyMarkovChainOrder + 1)];

    for (unsigned int i = 0; i < NbInd; i++) {
        InputFile.read(Ind, NbMarkers + 1);

        MoveForward(Ind);
        MoveBackward(Ind);
        if (DoProbas)
            WriteProbasImputation(Ind);
        if (CheckTauCoherence) {
            CheckTausFile << '\n' << '\n' << "Ind " << i << ":" << '\n';
            CheckTaus();
        }
        if (DoViterbi)
            Viterbi(Ind);
    }
    delete[] Ind;
    Ind = NULL;
    delete[] Forward;
    Forward = NULL;
    delete[] G;
    G = NULL;
    delete[] Tau;
    Tau = NULL;
    InputFile.close();
    ProbasCharsFile.close();
    ProbasHRNumbersFile.close();
    ProbasBinaryFile.close();

    if (DoProbas) {
        ProbasHRNumbersFile.close();
        ProbasBinaryFile.close();
        ProbasCharsFile.close();
        std::string Aux = BinaryProbasFileName + "__Aux3";
        Transpose(BinaryProbasFileName, Aux, sizeof(double),
                  NbMarkers * sizeof(double) + 1);
        FromBinToHR<double>(Aux, NumbersProbasFileName,
                            NbInd * sizeof(double) + 1);
        std::remove(Aux.c_str());
        std::remove(BinaryProbasFileName.c_str());
        // Now transposing the chars file of Probas and adding tabs.
        Aux = BinaryProbasFileName + "__Aux2";
        Transpose(CharsProbasFileName, Aux);
        AddTabs(Aux);
        std::remove(CharsProbasFileName.c_str());
        std::rename(Aux.c_str(), CharsProbasFileName.c_str());
    }
    if (DoViterbi) {
        ViterbiFile.close();
        std::string Aux = ViterbiFileName + "__Aux1";
        Transpose(ViterbiFileName, Aux);
        AddTabs(Aux);
        std::remove(ViterbiFileName.c_str());
        std::rename(Aux.c_str(), ViterbiFileName.c_str());
    }
    if (CheckTauCoherence)
        CheckTausFile.close();
}

/////////////////////
/////////////////////
////
////      Viterbi Imputation
////
/////////////////////
/////////////////////

void Imputator::Viterbi(char *Ind) {
    InitializeInitialProbabilities(Ind);

    // unsigned char PositionsOf2[MARKOVCHAINORDER];
    unsigned int Nb2 = 0;
    unsigned int n = 0;
    for (unsigned int BeginPosition = 0;
         BeginPosition < NbMarkers - MyMarkovChainOrder; BeginPosition++) {
        n = 0;
        Nb2 = 0;
        n = GetInitial(Ind + BeginPosition, PositionsOf2, Nb2,
                       MyMarkovChainOrder);
        // Computing new Forward
        // At the ned of next loop G should contain predecessor of each letter
        // at each locus...
        for (int i = 0; i < (1 << Nb2); i++) {
            unsigned int Letter = Fill(n, PositionsOf2, i, Nb2);
            unsigned int NewLetter0, NewLetter1;
            NewLetter0 = RightShift(Letter, 0, MyMarkovChainOrder);
            NewLetter1 = RightShift(Letter, 1, MyMarkovChainOrder);
            if ((Ind[BeginPosition + MyMarkovChainOrder] == '0') ||
                (Ind[BeginPosition + MyMarkovChainOrder] == '2'))
                if (GetForward(BeginPosition, Letter) *
                        MyTransitions[BeginPosition].Proba(Letter, 0) >
                    GetForward(BeginPosition + 1, NewLetter0)) {
                    GetForward(BeginPosition + 1, NewLetter0) =
                        GetForward(BeginPosition, Letter) *
                        MyTransitions[BeginPosition].Proba(Letter, 0);
                    GetG(BeginPosition + 1, NewLetter0) = Letter;
                }
            if ((Ind[BeginPosition + MyMarkovChainOrder] == '1') ||
                (Ind[BeginPosition + MyMarkovChainOrder] == '2'))
                if (GetForward(BeginPosition, Letter) *
                        MyTransitions[BeginPosition].Proba(Letter, 1) >
                    GetForward(BeginPosition + 1, NewLetter1)) {
                    GetForward(BeginPosition + 1, NewLetter1) =
                        GetForward(BeginPosition, Letter) *
                        MyTransitions[BeginPosition].Proba(Letter, 1);
                    GetG(BeginPosition + 1, NewLetter1) = Letter;
                }
        }
        // Normalizing Forward
        long double Somme = 0;
        for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
            Somme += GetForward(BeginPosition + 1, i);
        assert(Somme > 0);
        for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
            GetForward(BeginPosition + 1, i) /= Somme;
    }

    // Going backward...
    // First step: grab the last letter.
    int LastLetter = 0;
    int LocusIndex = NbMarkers - MyMarkovChainOrder;
    for (int Letter = 1; Letter < (1 << MyMarkovChainOrder); Letter++)
        if (GetForward(LocusIndex, Letter) > GetForward(LocusIndex, LastLetter))
            LastLetter = Letter;
    // Then decompose this last letter into the MARKOVCHAINORDER last chars of
    // Ind

    /*
    for (int DigitIndex = MyMarkovChainOrder - 1; DigitIndex >= 0 ;
    DigitIndex--)
    {
      char c;
      int Dig = LastLetter / (1 << DigitIndex);
      Dig %= 2;
      c = '0' + (Dig % 2);
      Ind[NbMarkers - MyMarkovChainOrder + Dig] = c;
    }
     */
    int LastLetterCopy = LastLetter;
    for (int DigitIndex = 0; DigitIndex < MyMarkovChainOrder; DigitIndex++) {
        char c = '0' + (LastLetterCopy % 2);
        int CurrentIndex = NbMarkers + DigitIndex - MyMarkovChainOrder;


        Ind[CurrentIndex] = '0' + (LastLetterCopy % 2);
        LastLetterCopy /= 2;
    }

    // Finaly grabbing the preceding letters
    int CurrentLetter = LastLetter;
    for (int Locus = NbMarkers - MyMarkovChainOrder - 1; Locus >= 0; Locus--) {
        CurrentLetter = GetG(Locus + 1, CurrentLetter);
        char c = '0' + (CurrentLetter % 2);
        Ind[Locus] = c;
    }
    ViterbiFile.write(Ind, NbMarkers);
    ViterbiFile << '\n';
}

/////////////////////
/////////////////////
////
////      Imputation by computation of probabilities for each locus
///(forward-backward method)
////
/////////////////////
/////////////////////

void Imputator::MoveForward(char *Ind) {
    InitializeInitialProbabilities(Ind);
    // unsigned char PositionsOf2[MARKOVCHAINORDER];
    unsigned int Nb2 = 0;
    unsigned int n = 0;
    for (unsigned int BeginPosition = 0;
         BeginPosition < NbMarkers - MyMarkovChainOrder; BeginPosition++) {
        n = GetInitial(Ind + BeginPosition, PositionsOf2, Nb2,
                       MyMarkovChainOrder);
        // Computing new Forward
        for (int i = 0; i < (1 << Nb2); i++) {
            unsigned int Letter = Fill(n, PositionsOf2, i, Nb2);
            unsigned int NewLetter0, NewLetter1;
            NewLetter0 = RightShift(Letter, 0, MyMarkovChainOrder);
            NewLetter1 = RightShift(Letter, 1, MyMarkovChainOrder);
            if ((Ind[BeginPosition + MyMarkovChainOrder] == '0') ||
                (Ind[BeginPosition + MyMarkovChainOrder] == '2'))
                GetForward(BeginPosition + 1, NewLetter0) +=
                    GetForward(BeginPosition, Letter) *
                    MyTransitions[BeginPosition].Proba(Letter, 0);
            if ((Ind[BeginPosition + MyMarkovChainOrder] == '1') ||
                (Ind[BeginPosition + MyMarkovChainOrder] == '2'))
                GetForward(BeginPosition + 1, NewLetter1) +=
                    GetForward(BeginPosition, Letter) *
                    MyTransitions[BeginPosition].Proba(Letter, 1);
        }

        // Normalizing Forward
        long double Somme = 0;
        for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
            Somme += GetForward(BeginPosition + 1, i);
        assert(Somme > 0);
        for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
            GetForward(BeginPosition + 1, i) /= Somme;
    }
}

void Imputator::MoveBackward(char *Ind) {
    // Initializing last Taus and G
    int BeginPosition = NbMarkers - MyMarkovChainOrder;
    for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
        GetTau(BeginPosition, i) = GetForward(BeginPosition, i);

    long double Somme = 0;
    for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
        Somme += GetTau(BeginPosition, i);
    assert(Somme > 0);
    for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
        GetTau(BeginPosition, i) /= Somme;

    unsigned int Nb2 = 0;
    unsigned int n = 0;

    for (int BeginPosition = NbMarkers - MyMarkovChainOrder - 1;
         BeginPosition >= 0; BeginPosition--) {
        n = GetInitial(Ind + BeginPosition + 1, PositionsOf2, Nb2,
                       MyMarkovChainOrder);
        // Updating G
        for (int i = 0; i < (1 << Nb2); i++) {
            unsigned int Letter = Fill(n, PositionsOf2, i, Nb2);
            unsigned int NewLetter0, NewLetter1,
                RightDigit = Letter / (1 << (MyMarkovChainOrder - 1));
            NewLetter0 = LeftShift(Letter, 0, MyMarkovChainOrder);
            NewLetter1 = LeftShift(Letter, 1, MyMarkovChainOrder);
            if ((Ind[BeginPosition] == '0') || (Ind[BeginPosition] == '2'))
                GetG(BeginPosition + 1, Letter) +=
                    GetForward(BeginPosition, NewLetter0) *
                    MyTransitions[BeginPosition].Proba(NewLetter0, RightDigit);
            if ((Ind[BeginPosition] == '1') || (Ind[BeginPosition] == '2'))
                GetG(BeginPosition + 1, Letter) +=
                    GetForward(BeginPosition, NewLetter1) *
                    MyTransitions[BeginPosition].Proba(NewLetter1, RightDigit);
        }

        Somme = 0;
        for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
            Somme += GetG(BeginPosition + 1, i);
        assert(Somme > 0);

        // Now updating Taus

        n = GetInitial(Ind + BeginPosition, PositionsOf2, Nb2,
                       MyMarkovChainOrder);
        for (int i = 0; i < (1 << Nb2); i++) {
            unsigned int Letter = Fill(n, PositionsOf2, i, Nb2);
            unsigned int NewLetter0, NewLetter1;
            NewLetter0 = RightShift(Letter, 0, MyMarkovChainOrder);
            NewLetter1 = RightShift(Letter, 1, MyMarkovChainOrder);
            if ((Ind[BeginPosition + MyMarkovChainOrder] == '0') ||
                (Ind[BeginPosition + MyMarkovChainOrder] == '2'))
                if (GetG(BeginPosition + 1, NewLetter0) > 0)
                    GetTau(BeginPosition, Letter) +=
                        GetTau(BeginPosition + 1, NewLetter0) *
                        GetForward(BeginPosition, Letter) *
                        MyTransitions[BeginPosition].Proba(Letter, 0) /
                        GetG(BeginPosition + 1, NewLetter0);
            if ((Ind[BeginPosition + MyMarkovChainOrder] == '1') ||
                (Ind[BeginPosition + MyMarkovChainOrder] == '2'))
                if (GetG(BeginPosition + 1, NewLetter1) > 0)
                    GetTau(BeginPosition, Letter) +=
                        GetTau(BeginPosition + 1, NewLetter1) *
                        GetForward(BeginPosition, Letter) *
                        MyTransitions[BeginPosition].Proba(Letter, 1) /
                        GetG(BeginPosition + 1, NewLetter1);
        }
        Somme = 0;
        for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
            Somme += GetTau(BeginPosition, i);
        assert(Somme > 0);
        for (int i = 0; i < (1 << MyMarkovChainOrder); i++)
            GetTau(BeginPosition, i) /= Somme;
    }
}

void Imputator::WriteProbasImputation(char *Ind) {
    double P1;
    for (unsigned int LetterPosition = 0; LetterPosition < NbMarkers;
         LetterPosition++) {
        if (LetterPosition + MyMarkovChainOrder < NbMarkers)
            P1 = ProbaToBe1FixedDigit(LetterPosition, 0);
        else
            P1 = ProbaToBe1FixedDigit(LetterPosition, LetterPosition -
                                                          NbMarkers +
                                                          MyMarkovChainOrder);
        char c = '.';
        if (P1 < 1 - Threshold)
            c = '0';
        else if (P1 > Threshold)
            c = '1';
        ProbasCharsFile << c;
        ProbasHRNumbersFile << P1 << '\t';
        ProbasBinaryFile.write((char *)&P1, sizeof(double));
    }
    ProbasCharsFile << '\n';
    ProbasHRNumbersFile << '\n';
    ProbasBinaryFile << '\n';
}
