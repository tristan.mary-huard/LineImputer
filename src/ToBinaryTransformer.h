//
//  ToBinaryTransformer.hpp
//  Impute
//
//  Created by Michel Koskas on 02/10/2016.
//  Copyright © 2016 INRA. All rights reserved.
//

#ifndef ToBinaryTransformer_hpp
#define ToBinaryTransformer_hpp

#include <stdio.h>
#include <string>

class Imputator;

class ToBinaryTransformer {
  public:
    Imputator *MyImputator;
    std::string InputFileName;
    std::string OutputFileName;
    ToBinaryTransformer();
    ToBinaryTransformer(std::string I, std::string O, Imputator *Im,
                        unsigned int &NbInd, unsigned int &NbMarkers);

  private:
    void TreatOneLine(char *BeginLine, char *EndLine, std::ofstream &OutputFile,
                      int &NbPoints, int &TotalDataSize, unsigned int &NbInd,
                      unsigned int &NbMarkers);
    int TreatBuffer(char *Buffer, int BufferSize, std::ofstream &OutputFile,
                    int &NbPoints, int &TotalDataSize, unsigned int &NbInd,
                    unsigned int &NbMarkers);
    void CreateFile(unsigned int &NbInd, unsigned int &NbMarkers);
};

#endif /* ToBinaryTransformer_hpp */
