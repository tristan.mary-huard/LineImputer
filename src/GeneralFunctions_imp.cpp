
#include "GeneralFunctions_imp.h"
#include "ToBinaryTransformer.h"
#include <assert.h>
#include <cstdlib>

unsigned long int GetFileSize(std::ifstream &File) {
    unsigned long int Answer = 0;
    unsigned long int InitialPosition = File.tellg();
    File.seekg(0, std::ios::end);
    Answer = File.tellg();
    File.seekg(InitialPosition, std::ios::beg);
    return Answer;
}

void AddTabs(std::string FileName) {
    std::string OutputFileName = FileName + "__Aux0";
    std::ifstream InputFile(FileName.c_str(),
                            std::ifstream::in | std::ifstream::binary);
    assert(InputFile.is_open());
    std::ofstream OutputFile(OutputFileName.c_str(),
                             std::ofstream::out | std::ofstream::binary);
    assert(OutputFile.is_open());
    unsigned int NbCols, FileSize, NbLines;
    NbCols = LineLength(InputFile, FileSize) - 1;
    assert(FileSize % (NbCols + 1) == 0);
    NbLines = FileSize / (NbCols + 1);
    char *InputBuffer = new char[NbCols + 1];
    char *OutputBuffer = new char[2 * NbCols];
    OutputBuffer[2 * NbCols - 1] = '\n';

    for (unsigned int NumLine = 0; NumLine < NbLines; NumLine++) {
        InputFile.read(InputBuffer, NbCols + 1);
        for (unsigned int i = 0; i < NbCols; i++) {
            OutputBuffer[2 * i] = InputBuffer[i];
            if (i < NbCols - 1)
                OutputBuffer[2 * i + 1] = '\t';
        }
        OutputFile.write(OutputBuffer, 2 * NbCols);
    }
    delete[] InputBuffer;
    delete[] OutputBuffer;
    InputFile.close();
    OutputFile.close();
    std::remove(FileName.c_str());
    std::rename(OutputFileName.c_str(), FileName.c_str());
}

// In the next function:
// n is an integer containing 0 1 or 2 digits where the 2 have been replaced
// with 0. Positions is the array of the positions of the digits 2 that have
// been replaces by 2. NbPositions is the number of positions. Value is an
// integer 0 <= Value < (1 << NbPositions) It contains the digits that should be
// written in n For instance if inout data is: n = 01001010 01010010 10100101
// 01000101 P = ...X...X X...XX.. .X....X. X.....X. (NbPositions is 9) and
// Values = 00000000 00000000 00000001 10101011
//
// Then the returned result should be:
//
//          n = 01001010 01010010 10100101 01000101
//          P = ...X...X X...XX.. .X....X. X.....X.
// and Values = 00000000 00000000 00000001 10101011
//          P = ...1...1 0...10.. .1....0. 1.....1.
//     Answer = 01011011 01011010 11100101 11000111

// (bit de poids faible à m'indice 0)
//
unsigned int GetInitial(char *Ind, unsigned char *PositionsOf2,
                        unsigned int &Nb2, unsigned short int LengthInd) {
    unsigned int n = 0;
    Nb2 = 0;
    for (int i = 0; i < LengthInd; i++) {
        if (Ind[i] == '2') {
            PositionsOf2[Nb2] = i;
            Nb2++;
        }
        if (Ind[i] == '1')
            n += 1 << i;
    }
    return n;
}

unsigned int Fill(unsigned int n, unsigned char *Positions, unsigned int Values,
                  unsigned int NbPositions) {
    unsigned int Answer = n;
    for (unsigned int i = 0; i < NbPositions; i++) {
        unsigned int Divisor = 1 << Positions[i];
        unsigned int Remaining = Answer % Divisor;
        Answer /= (2 * Divisor);
        Answer *= 2;
        Answer += ((Values / (1 << i)) % 2);
        Answer *= Divisor;
        Answer += Remaining;
    }
    return Answer;
}

unsigned int RightShift(unsigned int LetterAtLeft, unsigned int RightDigit,
                        unsigned short int MarkovChainOrder) {
    return (LetterAtLeft / 2) + RightDigit * (1 << (MarkovChainOrder - 1));
}

unsigned int LeftShift(unsigned int LetterAtRight, unsigned int LeftDigit,
                       unsigned short int MarkovChainOrder) {
    unsigned int Answer = LetterAtRight % (1 << (MarkovChainOrder - 1));
    Answer *= 2;
    Answer += LeftDigit;
    return Answer;
}

long double ProbaToBe1LittleEndian(long double *Tau,
                                   unsigned short int MarkovChainOrder) {
    long double Answer = 0;
    for (int LetterIndex = 1; LetterIndex < (1 << MarkovChainOrder);
         LetterIndex += 2)
        Answer += Tau[LetterIndex];
    return Answer;
}

long double ProbaToBe1BigEndian(long double *Tau,
                                unsigned short int MarkovChainOrder) {
    long double Answer = 0;

    for (int LetterIndex = 1 << (MarkovChainOrder - 1);
         LetterIndex < (1 << MarkovChainOrder); LetterIndex++)
        Answer += Tau[LetterIndex];
    return Answer;
}

int CompatibleLetters(unsigned int Basis2, unsigned int Basis3,
                      unsigned short int MarkovChainOrder) {
    unsigned int Left = Basis2, Right = Basis3;
    for (int i = 0; i < MarkovChainOrder; i++) {
        unsigned int L = Left % 2, R = Right % 3;
        if (L + R == 1)
            return 0;
    }
    return 1;
}

void FromVCFToMatrix(std::string InputFileName, std::string OutputFileName,
                     unsigned int &NbInd, unsigned int &NbMarkers) {
    ToBinaryTransformer B(InputFileName, OutputFileName, NULL, NbInd,
                          NbMarkers);
}

void Transpose(std::string InputFileName, std::string OutputFileName,
               unsigned int ElementSize, unsigned int LineL) {
    unsigned int AllowedMemory = 1000000000;

    std::ifstream InputFile(InputFileName.c_str(),
                            std::ifstream::in | std::ifstream::binary);
    assert(InputFile.is_open());
    std::ofstream OutputFile(OutputFileName.c_str(),
                             std::ofstream::out | std::ofstream::binary);
    assert(OutputFile.is_open());

    unsigned int InputFileSize = 0;
    // '\n' is included while counting FormerNbCharsPerLine
    unsigned int FormerNbCharsPerLine = LineL;
    if (FormerNbCharsPerLine == 0)
        FormerNbCharsPerLine = LineLength(InputFile, InputFileSize);
    if (InputFileSize == 0)
        InputFileSize = (unsigned int)GetFileSize(InputFile);

    assert(InputFileSize % FormerNbCharsPerLine == 0);
    unsigned int FormerNbLines = InputFileSize / FormerNbCharsPerLine;
    assert((FormerNbCharsPerLine - 1) % ElementSize == 0);
    unsigned int FormerNbColumns = (FormerNbCharsPerLine - 1) / ElementSize;
    unsigned int NewNbLines = FormerNbColumns;
    unsigned int NewLineSize = FormerNbLines * ElementSize + 1;

    unsigned int BufferSize = AllowedMemory / (sizeof(char *) + NewNbLines);
    BufferSize = BufferSize - (BufferSize % ElementSize);
    if (BufferSize == 0)
        BufferSize = ElementSize;
    char **Buffer = new char *[NewNbLines];
    for (unsigned int i = 0; i < NewNbLines; i++)
        Buffer[i] = new char[BufferSize];

    unsigned int IndexInBuffers = 0, NumBuffer = 0;
    char *OneLine = new char[FormerNbCharsPerLine];
    for (unsigned int CurrentLine = 0; CurrentLine < FormerNbLines;
         CurrentLine++) {
        InputFile.read(OneLine, FormerNbCharsPerLine * sizeof(char));
        for (unsigned int i = 0; i < NewNbLines;
             i++) // -1 to forget the last char, which is '\n'
            for (unsigned int k = 0; k < ElementSize; k++)
                Buffer[i][ElementSize * IndexInBuffers + k] =
                    OneLine[ElementSize * i + k];
        IndexInBuffers++;
        if (IndexInBuffers * ElementSize >= BufferSize) {
            for (unsigned int i = 0; i < NewNbLines; i++) {
                OutputFile.seekp(i * NewLineSize + NumBuffer * BufferSize);
                OutputFile.write(Buffer[i],
                                 IndexInBuffers * ElementSize * sizeof(char));
            }
            NumBuffer++;
            IndexInBuffers = 0;
        }
    }
    // Now flushing the buffers
    if (IndexInBuffers > 0)
        for (unsigned int i = 0; i < NewNbLines; i++) {
            OutputFile.seekp(i * NewLineSize + NumBuffer * BufferSize);
            OutputFile.write(Buffer[i],
                             IndexInBuffers * ElementSize * sizeof(char));
        }
    // Now adding the '\n' char at the end of each line
    char c = '\n';
    for (unsigned int i = 0; i < NewNbLines; i++) {
        OutputFile.seekp(i * NewLineSize + NewLineSize - 1);
        OutputFile.write(&c, 1 * sizeof(char));
    }
    InputFile.close();
    OutputFile.close();
    delete[] OneLine;
    for (unsigned int i = 0; i < FormerNbColumns; i++)
        delete[] Buffer[i];
    delete[] Buffer;
}

// This function returns true if the buffer is coherent between indices First
// and Last BOTH INCLUDED
bool CheckBuffer(char *Buffer, int First, int Last, int &ErrIndex) {
    for (int i = First; i < Last; i++)
        if ((Buffer[i] == '1') &&
            ((Buffer[i + 1] == '3') || (Buffer[i + 1] == '4'))) {
            ErrIndex = i;
            return false;
        } else if ((Buffer[i] == '2') &&
                   ((Buffer[i + 1] == '1') || (Buffer[i + 1] == '2'))) {
            ErrIndex = i;
            return false;
        } else if ((Buffer[i] == '3') &&
                   ((Buffer[i + 1] == '3') || (Buffer[i + 1] == '4'))) {
            ErrIndex = i;
            return false;
        } else if ((Buffer[i] == '4') &&
                   ((Buffer[i + 1] == '1') || (Buffer[i + 1] == '2'))) {
            ErrIndex = i;
            return false;
        }
    return true;
}

int RemoveTabs(char *Buffer, int BufferSize) {
    int WritePosition = 0;
    for (int i = 0; i < BufferSize; i++)
        if (Buffer[i] != '\t') {
            Buffer[WritePosition] = Buffer[i];
            WritePosition++;
        }
    return WritePosition;
}

void CheckFile(std::ifstream &File, std::string OutputFileName) {

    std::ofstream OutputFile(OutputFileName.c_str(),
                             std::ofstream::out | std::ofstream::binary);

    bool Correct = true;
    char *Buffer;
    unsigned int FileSize;
    unsigned int LineL = LineLength(File, FileSize);
    Buffer = new char[LineL];
    File.seekg(0, std::ios::beg);
    if (FileSize % LineL != 0)
        for (unsigned int i = 0; i < (FileSize + LineL - 1) / LineL; i++) {
            File.read(Buffer, LineL);
            for (unsigned int j = 0; j < LineL - 1; j++)
                if (Buffer[j] == '\n') {
                    Correct = false;
                    for (unsigned int k = j + 1; k < LineL; k++)
                        Buffer[k - j - 1] = Buffer[k];
                    File.read(Buffer + LineL - j, (j + 1));
                    i++;
                    OutputFile << i + 1 << '\t';
                    break;
                }
            if (Buffer[LineL - 1] != '\n') {
                Correct = false;
            }
        }
    delete[] Buffer;
    if (Correct)
        OutputFile << '\n' << '\n';
    OutputFile.close();
}

// returns the length of the line, '\n' included
unsigned int LineLength(std::ifstream &InputFile, unsigned int &FileSize) {
    unsigned int Res = -1;
    InputFile.seekg(0, std::ios::end);
    FileSize = (unsigned int)InputFile.tellg();
    unsigned int BufSize = 1000000;
    if (BufSize > FileSize)
        BufSize = FileSize;
    char *Buffer = new char[BufSize];
    InputFile.seekg(0, std::ios::beg);
    InputFile.read(Buffer, BufSize * sizeof(char));

    bool ResultFound = false;

    unsigned int FirstChar = 0;
    while ((!ResultFound) && (FirstChar < FileSize)) {
        for (unsigned int i = 0; i < BufSize; i++)
            if (Buffer[i] == '\n') {
                Res = FirstChar + i + 1;
                ResultFound = true;
                break;
            }
        if (!ResultFound) {
            FirstChar += BufSize;
            if (FileSize >= BufSize + FirstChar)
                InputFile.read(Buffer, BufSize * sizeof(char));
            else
                InputFile.read(Buffer, (FileSize - FirstChar) * sizeof(char));
        }
    }
    delete[] Buffer;
    InputFile.seekg(0, std::ios::beg);
    return Res;
}

// Get the position of the beginning of the next value in the line. This
// function is applied when the values have different lengths.
int get_end_metadata_position(std::string Line, int line_length,
                              int nb_supp_elements) {
    int Answer = 0;
    int i = 0;
    while ((i < nb_supp_elements) && (Answer < line_length)) {
        Answer++;
        if (Line[Answer] == '\t')
            i++;
    }
    return Answer;
}

int get_next_value_position(std::string Line, int FirstPos, int line_length) {
    int Answer = FirstPos;
    while ((Line[Answer] != '\t') && (Answer < line_length))
        Answer++;
    return Answer + 1;
}

void ImpThreshold(std::string probas_filename, std::string output_filename,
                  double threshold, int nb_supp_elements, int nb_cols,
                  bool replace_values, bool do_threshold) {
    std::ifstream probas_file(probas_filename.c_str(),
                              std::ifstream::in | std::ifstream::binary);
    assert(probas_file.is_open());
    std::ofstream output_file(
        output_filename.c_str(),
        std::ofstream::out | std::ofstream::binary); //, std::ios_base::app);
    assert(output_file.is_open());

    std::string probas_line, vcf_to_file, value;
    int line_length, vcf_end_position, begin_value, begin_next_value;
    unsigned int FileSize, NbReadChars = 0;
    double pb_value;
    probas_file.seekg(0, std::ios::end);
    FileSize = (unsigned int)probas_file.tellg();
    probas_file.seekg(0, std::ios::beg);

    // while (! probas_file.eof())
    if (replace_values == true) {

        int NbReadValues = 0;
        while (NbReadChars < FileSize) {

            char CurrentChar;

            probas_file >> CurrentChar;
            NbReadChars += 2;
            if (CurrentChar == '1')
                output_file << "1/1";
            else if (CurrentChar == '0')
                output_file << "0/0";
            else
                output_file << "./.";
            // probas_file >> CurrentChar; // We read a second char to drop \t
            // or \n from the input file

            NbReadValues++;
            if (NbReadValues % nb_cols != 0)
                output_file << '\t';
            else
                output_file << '\n';
        }
    }

    if (do_threshold == true) {

        getline(probas_file, probas_line);
        output_file << probas_line << '\n';
        while (probas_line[0] == '#' && probas_line[1] == '#') {
            getline(probas_file, probas_line);
            output_file << probas_line << '\n';
        }

        while (!probas_file.eof()) {
            getline(probas_file, probas_line);
            line_length = probas_line.length();
            vcf_end_position = get_end_metadata_position(
                probas_line, line_length, nb_supp_elements);
            vcf_to_file = probas_line.substr(0, vcf_end_position);
            output_file << vcf_to_file;
            begin_next_value = vcf_end_position + 1;

            while (begin_next_value < line_length) {
                output_file << '\t';
                begin_value = begin_next_value;
                begin_next_value = get_next_value_position(
                    probas_line, begin_value, line_length);
                value = probas_line.substr(begin_value,
                                           begin_next_value - begin_value - 1);
                pb_value = ::atof(value.c_str());
                if (pb_value > threshold)
                    output_file << "1/1";
                else if (pb_value < 1 - threshold)
                    output_file << "0/0";
                else
                    output_file << "./.";
            }
            output_file << "\n";
        }
    }

    probas_file.close();
    output_file.close();
}

void txt_to_vcf(std::string txt_filename, std::string vcf_filename,
                std::string output_filename, int nb_supp_elements, int nb_cols,
                bool replace_values) {

    if (replace_values == true) {
        std::string txt_filename_replaced =
            txt_filename.substr(0, txt_filename.length() - 4) + "_adjusted.txt";
        ImpThreshold(txt_filename, txt_filename_replaced, 0, nb_supp_elements,
                     nb_cols, true, false);
        txt_filename = txt_filename_replaced;
    }

    std::ifstream txt_file(txt_filename.c_str(),
                           std::ifstream::in | std::ifstream::binary);
    assert(txt_file.is_open());
    std::ifstream vcf_file(vcf_filename.c_str(),
                           std::ifstream::in | std::ifstream::binary);
    assert(vcf_file.is_open());
    std::ofstream output_file(output_filename.c_str(),
                              std::ofstream::out | std::ofstream::binary);
    assert(output_file.is_open());
    std::string vcf_line, vcf_to_file, txt_line;
    int line_length, vcf_end_position;

    getline(vcf_file, vcf_line);
    while (vcf_line[0] == '#') {
        output_file << vcf_line << '\n';
        getline(vcf_file, vcf_line);
    }
    line_length = vcf_line.length();
    getline(txt_file, txt_line);
    for (int i = 0; i < nb_supp_elements; i++) {
        vcf_end_position =
            get_end_metadata_position(vcf_line, line_length, nb_supp_elements);
        vcf_to_file = vcf_line.substr(0, vcf_end_position);
    }
    output_file << vcf_to_file << '\t' << txt_line << '\n';

    while (!txt_file.eof() && !vcf_file.eof()) {
        getline(txt_file, txt_line);
        getline(vcf_file, vcf_line);
        line_length = vcf_line.length();
        for (int i = 0; i < nb_supp_elements; i++) {
            vcf_end_position = get_end_metadata_position(vcf_line, line_length,
                                                         nb_supp_elements);
            vcf_to_file = vcf_line.substr(0, vcf_end_position);
        }
        if (vcf_to_file.length() > 0 || txt_line.length() > 0) {
            output_file << vcf_to_file << '\t' << txt_line << '\n';
        }
    }
}
