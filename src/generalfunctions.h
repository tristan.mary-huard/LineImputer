#ifndef GENERALFUNCTIONS_H
#define GENERALFUNCTIONS_H

#include "couple.h"
#include "generalfunctions.h"
#include "triple.h"
#include <algorithm>
#include <assert.h>
#include <fstream>
#include <iostream>
#include <list>
#include <math.h>
#include <string>

bool CommentLine(std::string Line, char CommentChar);

int GetNextColnamePosition(std::string Line, int FirstPos, int line_length,
                           char separator);

void make_list_of_colnames(std::string file_name, std::string output_path,
                           std::ofstream &log_file,
                           std::list<std::string> &list_supp_colnames,
                           std::list<std::string> &list_main_colnames,
                           char comment_sign = '#', int nb_supp_elements = 9,
                           char separator = '\t');

std::list<std::string> make_list_of_rownames(std::string file_data,
                                             std::string output_path,
                                             std::ofstream &log_file,
                                             char comment_sign = '#',
                                             char separator = '\t');

Couple *enumerate_list(std::list<std::string> list_main,
                       std::list<std::string> list_supp, bool IsSorted = false);

std::list<Triple> merge_list(Couple *ind_list_1, Couple *ind_list_2,
                             int list_size_1, int list_size_2);

std::string take_value_from_line(std::string line, int current_col,
                                 int &OffsetSuppCols, int nb_supp_elements = 9,
                                 int value_length = 3, char separator = '\t',
                                 std::string unknown = "./.");

void fusion(std::string liststring, std::string filename_1,
            std::string filename_2, std::string output_path,
            std::string output_filename, std::ofstream &log_file,
            std::list<Triple> merged_list_rows,
            std::list<Triple> merged_list_supp_cols,
            std::list<Triple> merged_list_main_cols,
            char comment_sign = '#',
            std::string unknown = "./.", char separator = '\t',
            int nb_supp_elements = 9, int value_length = 3,
            std::string file_extension = "vcf", int nb_supp_lines = 5,
            bool last_iteration = true);

void process_data(std::string liststring, std::string file_name_1,
                  std::string file_name_2, std::string output_path,
                  std::string output_filename, std::string unknown = "./.",
                  char comment_sign = '#', char separator = '\t',
                  bool IsSorted = false, int nb_supp_elements = 9,
                  int value_length = 3, std::string file_extension = "vcf",
                  int nb_supp_lines = 5, bool last_iteration = true,
                  int num_col_sort = 2);

std::string CreateOffsetsFile(std::string filename,
                              std::string file_extension = "vcf");

std::string GetLine(std::ifstream &DataFile, std::ifstream &OffsetsFile,
                    int NumLine, int nb_supp_lines);

#endif // GENERALFUNCTIONS_H
