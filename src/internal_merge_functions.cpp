// [[Rcpp::plugins(cpp11)]]
#include <Rcpp.h>
using namespace Rcpp;
#include "couple.h"
#include "generalfunctions.h"
#include "triple.h"
#include <algorithm>
#include <assert.h>
#include <ctime>
#include <fstream>
#include <iostream>
#include <list>
#include <math.h>
#include <string>

// [[Rcpp::export]]
void fusionCpp(CharacterVector liststring, CharacterVector output_path,
               CharacterVector output_filename, CharacterVector input_file1,
               CharacterVector input_file2, CharacterVector unk_sign,
               CharacterVector comment_sign, CharacterVector separator,
               LogicalVector issorted, IntegerVector nb_supp_elements,
               IntegerVector value_length, CharacterVector file_extension,
               IntegerVector nb_supp_lines, LogicalVector last_iteration,
               IntegerVector num_col_sort) {
    std::string f0 = Rcpp::as<std::string>(liststring);
    std::string f1 = Rcpp::as<std::string>(output_path);
    std::string f2 = Rcpp::as<std::string>(output_filename);
    std::string f3 = Rcpp::as<std::string>(input_file1);
    std::string f4 = Rcpp::as<std::string>(input_file2);
    std::string f5 = Rcpp::as<std::string>(unk_sign);
    char f6 = Rcpp::as<char>(comment_sign);
    char f7 = Rcpp::as<char>(separator);
    bool f8 = Rcpp::as<bool>(issorted);
    int f9 = Rcpp::as<int>(nb_supp_elements);
    int f10 = Rcpp::as<int>(value_length);
    std::string f11 = Rcpp::as<std::string>(file_extension);
    int f12 = Rcpp::as<int>(nb_supp_lines);
    bool f13 = Rcpp::as<bool>(last_iteration);
    int f14 = Rcpp::as<int>(num_col_sort);
    process_data(f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13,
                 f14);
}
