#include "generalfunctions.h"
#include <algorithm>
#include <assert.h>
#include <fstream>
#include <iostream>
#include <list>
#include <math.h>
#include <string>
#include <vector>

#include "couple.h"
#include "triple.h"

// Whether the line is line of comments/metadate or not
bool CommentLine(std::string Line, char CommentChar) {
    if (Line[0] != CommentChar)
        return false;
    if (Line[1] == CommentChar)
        return true;
    return false;
}

// Get the position of the beginning of the next value in the line. This
// function is applied when the values have different lengths.
int GetNextColnamePosition(std::string Line, int FirstPos, int line_length,
                           char separator) {
    int Answer = FirstPos;
    while ((Line[Answer] != separator) && (Answer < line_length))
        Answer++;
    return Answer + 1;
}

// Make the list of colnames of the file
void make_list_of_colnames(std::string file_name, std::string output_path,
                           std::ofstream &log_file,
                           std::list<std::string> &list_supp_colnames,
                           std::list<std::string> &list_main_colnames,
                           char comment_sign, int nb_supp_elements,
                           char separator) {
    int begin_colname = 0, begin_next_colname = 0, nb_columns = 0;
    std::string colname, line_of_colnames;
    bool comment_line = true;
    assert(log_file.is_open());

    std::ifstream myfile(file_name.c_str(),
                         std::ifstream::in | std::ifstream::binary);
    //    std::ofstream colnames_file("/.../colnames.txt");
    assert(myfile.is_open());
    //    assert(colnames_file.is_open());

    while ((!myfile.eof()) && (comment_line)) {
        getline(myfile, line_of_colnames);
        comment_line = CommentLine(line_of_colnames, comment_sign);
    }
    int line_length = line_of_colnames.length();
    for (int i = 0; i < nb_supp_elements; i++) {
        begin_colname = begin_next_colname;
        begin_next_colname = GetNextColnamePosition(
            line_of_colnames, begin_colname, line_length, separator);
        colname = line_of_colnames.substr(begin_colname, begin_next_colname -
                                                             begin_colname - 1);
        if (*(colname.rbegin()) == '\r')
            colname.pop_back();
        list_supp_colnames.push_back(colname.c_str());
    }
    begin_colname = begin_next_colname;
    begin_next_colname = GetNextColnamePosition(line_of_colnames, begin_colname,
                                                line_length, separator);
    colname = line_of_colnames.substr(begin_colname,
                                      begin_next_colname - begin_colname - 1);
    if (*(colname.rbegin()) == '\r')
        colname.pop_back();
    list_main_colnames.push_back(colname.c_str());
    nb_columns++;
    while (begin_next_colname < line_length) {
        begin_colname = begin_next_colname;
        begin_next_colname = GetNextColnamePosition(
            line_of_colnames, begin_colname, line_length, separator);
        colname = line_of_colnames.substr(begin_colname, begin_next_colname -
                                                             begin_colname - 1);
        if (*(colname.rbegin()) == '\r')
            colname.pop_back();
        list_main_colnames.push_back(colname.c_str());
        nb_columns++;
    }
    myfile.close();

    log_file << "     - Number of colnames: "
             << list_supp_colnames.size() + list_main_colnames.size()
             << std::endl;
}

// Make the list of rownames of the file
std::list<std::string> make_list_of_rownames(std::string file_name,
                                             std::string output_path,
                                             std::ofstream &log_file,
                                             char comment_sign, char separator,
                                             int num_col_sort) {
    std::string rowname, line_of_rownames;
    bool comment_line = true;
    std::list<std::string> list_of_rownames;

    std::ifstream myfile(file_name.c_str(),
                         std::ifstream::in | std::ifstream::binary);
    //    std::ofstream rownames_file ("/.../rownames.txt");
    assert(myfile.is_open());
    assert(log_file.is_open());

    while ((!myfile.eof()) && (comment_line)) {
        getline(myfile, line_of_rownames);
        comment_line = CommentLine(line_of_rownames, comment_sign);
    }

    while (!myfile.eof()) {
        getline(myfile, line_of_rownames);
        if (CommentLine(line_of_rownames, comment_sign))
            continue;
        int line_length = line_of_rownames.length();
        if (line_length == 0)
            continue;
        int NextP = 0, P = 0;
        for (int i = 0; i < num_col_sort; i++) {
            P = NextP;
            NextP = GetNextColnamePosition(line_of_rownames, P, line_length,
                                           separator);
        }
        rowname = line_of_rownames.substr(P, NextP - P - 1);
        list_of_rownames.push_back(rowname);
    }
    myfile.close();

    log_file << "     - Number of rownames: " << list_of_rownames.size() << "."
             << std::endl;
    return (list_of_rownames);
}

void log_duplicated_rowname(std::ofstream &log_file,
                            std::list<std::string> &list_of_rownames) {
    bool header_not_printed = true;
    unsigned int number_dup = 0;
    std::string last_rowname;
    for (std::list<std::string>::iterator rni = list_of_rownames.begin();
         rni != list_of_rownames.end(); rni++) {
        if (*rni == last_rowname) {
            number_dup += 1;
        } else {
            if (number_dup > 0) {
                if (header_not_printed) {
                    header_not_printed = false;
                    log_file << "     - Duplicated pos:" << std::endl;
                }
                log_file << "       - " << last_rowname << " (" << number_dup
                         << ")" << std::endl;
                number_dup = 0;
            }
        }
        last_rowname = *rni;
    }
    if (number_dup > 0) {
        if (header_not_printed) {
            header_not_printed = false;
            log_file << "     - Duplicated pos:" << std::endl;
        }
        log_file << "       - " << last_rowname << " (" << number_dup << ")"
                 << std::endl;
        number_dup = 0;
    }
}

// Enumerate each element of the list (of colnames/rownames) and sort the list
// in alphabetic order
Couple *enumerate_list(std::list<std::string> list_main,
                       std::list<std::string> list_supp, bool IsSorted) {

    int CurrentIndex = 0;
    Couple *Answer = new Couple[list_main.size() + list_supp.size()];
    std::string name;

    for (std::list<std::string>::iterator I = list_supp.begin();
         I != list_supp.end(); I++, CurrentIndex++) {
        Answer[CurrentIndex].Initialize(*I, CurrentIndex);
    }
    for (std::list<std::string>::iterator I = list_main.begin();
         I != list_main.end(); I++, CurrentIndex++) {
        Answer[CurrentIndex].Initialize(*I, CurrentIndex);
    }

    if (IsSorted == false)
        std::sort(Answer + list_supp.size(),
                  Answer + list_supp.size() + list_main.size());

    return Answer;
}

// Make one list from two by merging them
std::list<Triple> merge_list(Couple *ind_list_1, Couple *ind_list_2,
                             int list_size_1, int list_size_2) {

    int i = 0, j = 0, current_index_1, current_index_2;
    std::string current_element, current_element_1, current_element_2;
    std::list<Triple> merged_indexed_list;
    Triple new_triple;
    //    std::ofstream triple_file ("/.../triples.txt");

    while ((i < list_size_1) && (j < list_size_2)) {
        current_element_1 = ind_list_1[i].GetName();
        current_element_2 = ind_list_2[j].GetName();
        if (current_element_1 == current_element_2) {
            current_element = current_element_1;
            current_index_1 = ind_list_1[i].GetIndex();
            current_index_2 = ind_list_2[j].GetIndex();
            i++;
            j++;
        } else if (ind_list_1[i] < ind_list_2[j]) {
            current_element = current_element_1;
            current_index_1 = ind_list_1[i].GetIndex();
            current_index_2 = -1;
            i++;
        } else { // if (current_element_1 > current_element_2) {
            current_element = current_element_2;
            current_index_1 = -1;
            current_index_2 = ind_list_2[j].GetIndex();
            j++;
        }
        new_triple.Initialize(current_element, current_index_1,
                              current_index_2);
        merged_indexed_list.push_back(new_triple);

        //        triple_file << current_element << '\t' << current_index_1 <<
        //        '\t' << current_index_2 << std::endl;
    }

    while (i < list_size_1) { // if the list_1 is longer than the list_2
        current_element = ind_list_1[i].GetName();
        current_index_1 = ind_list_1[i].GetIndex();
        current_index_2 = -1;
        new_triple.Initialize(current_element, current_index_1,
                              current_index_2);
        merged_indexed_list.push_back(new_triple);
        //        triple_file << current_element << '\t' << current_index_1 <<
        //        '\t' << current_index_2 << std::endl;
        i++;
    }

    while (j < list_size_2) { // if the list_2 is longer than the list_1
        current_element = ind_list_2[j].GetName();
        current_index_1 = -1;
        current_index_2 = ind_list_2[j].GetIndex();
        new_triple.Initialize(current_element, current_index_1,
                              current_index_2);
        merged_indexed_list.push_back(new_triple);
        //        triple_file << current_element << '\t' << current_index_1 <<
        //        '\t' << current_index_2 << std::endl;
        j++;
    }

    //    triple_file.close();
    return merged_indexed_list;
}

int infer_nb_supp_lines(std::string &filename, char comment_sign) {
    int ret = 0;
    bool last_is_LF = true;
    bool last_is_first_of_line = false;
    std::ifstream ifs(filename.c_str(),
                      std::ifstream::in | std::ifstream::binary);
    while (true) {
        char c;
        ifs.get(c);
        if (last_is_first_of_line) {
            if (c != comment_sign)
                return ret;
        }
        last_is_first_of_line = false;
        if (last_is_LF) {
            if (c == comment_sign) {
                ret += 1;
                last_is_first_of_line = true;
            } else
                return ret;
        }
        if (c == '\n')
            last_is_LF = true;
        else
            last_is_LF = false;
    }
}

// Merge two files in one using initial files and merged lists of rownames and
// colnames
void fusion(std::string liststring, std::string filename_1,
            std::string filename_2, std::string output_path,
            std::string output_filename, std::ofstream &log_file,
            std::list<Triple> merged_list_rows,
            std::list<Triple> merged_list_supp_cols, 
            std::list<Triple> merged_list_main_cols, 
            char comment_sign,
            std::string main_unknown, char separator, int nb_supp_elements,
            int value_length, std::string file_extension, int nb_supp_lines,
            bool last_iteration) {

    int current_row_1, current_row_2, current_col_1, current_col_2;
    std::string current_line_1, current_line_2, value, value_1, value_2;
    std::ofstream output_file((output_path + output_filename).c_str(),
                              std::ofstream::out | std::ofstream::binary);
    std::ofstream incoherence_file((output_path + "Incoherences.txt").c_str(),
                                   std::ios_base::app);

    std::string OfsFileName1 = CreateOffsetsFile(filename_1, file_extension);
    std::string OfsFileName2 = CreateOffsetsFile(filename_2, file_extension);
    std::ifstream VCFFile1(filename_1.c_str(),
                           std::ifstream::in | std::ifstream::binary);
    std::ifstream VCFFile2(filename_2.c_str(),
                           std::ifstream::in | std::ifstream::binary);
    std::ifstream OfsFile1(OfsFileName1.c_str(),
                           std::ifstream::in | std::ifstream::binary);
    std::ifstream OfsFile2(OfsFileName2.c_str(),
                           std::ifstream::in | std::ifstream::binary);

    assert(output_file.is_open());
    assert(incoherence_file.is_open());

    if (last_iteration == true)
        incoherence_file << "Rowname" << '\t' << "Colname" << std::endl;

    output_file << comment_sign << comment_sign << "Merged files:" << '\n';
    output_file << comment_sign << comment_sign << liststring << '\n';
    output_file << comment_sign << comment_sign << '\n';
    output_file << comment_sign << comment_sign << '\n';

    std::list<Triple> merged_list_cols;
    for(std::list<Triple>::iterator I = merged_list_supp_cols.begin();
            I!=merged_list_supp_cols.end();
            I++)
        merged_list_cols.push_back(*I);
    for(std::list<Triple>::iterator I = merged_list_main_cols.begin();
            I!=merged_list_main_cols.end();
            I++)
        merged_list_cols.push_back(*I);

    int nb_supp_lines1 = infer_nb_supp_lines(filename_1, comment_sign);
    int nb_supp_lines2 = infer_nb_supp_lines(filename_2, comment_sign);
    for (std::list<Triple>::iterator I = merged_list_cols.begin();
         I != merged_list_cols.end(); I++) {
        output_file << (*I).GetName();
        I++;
        if (I != merged_list_cols.end())
            output_file << separator;
        else
            output_file << '\n';
        I--;
    }

    for (std::list<Triple>::iterator I = merged_list_rows.begin();
         I != merged_list_rows.end(); I++) {
        current_row_1 = (*I).GetIndex_1();
        current_row_2 = (*I).GetIndex_2();
        int OffsetSuppCols_1 = -1;
        int OffsetSuppCols_2 = -1;
        if (current_row_1 != -1 && current_row_2 == -1) {
            current_line_1 =
                GetLine(VCFFile1, OfsFile1, current_row_1, nb_supp_lines1);

            unsigned int colnum=0;
            for (std::list<Triple>::iterator J = merged_list_cols.begin();
                 J != merged_list_cols.end(); J++) {
                std::string unknown;
                if(colnum>=merged_list_supp_cols.size())
                    unknown = main_unknown;
                else
                    unknown = "[conflict]";
                colnum++;

                current_col_1 = (*J).GetIndex_1();
                value = take_value_from_line(current_line_1, current_col_1,
                                             OffsetSuppCols_1, nb_supp_elements,
                                             value_length, separator, unknown);

                if (last_iteration == true && value == "0/1") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }
                if (last_iteration == true && value == "2/2") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }

                if (last_iteration == true && value == "1/0") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }

                J++;
                if (J != merged_list_cols.end())
                    output_file << value << separator;
                else
                    output_file << value << '\n';
                J--;
            }

        } else if (current_row_1 == -1 && current_row_2 != -1) {
            current_line_2 =
                GetLine(VCFFile2, OfsFile2, current_row_2, nb_supp_lines2);
            unsigned int colnum=0;
            for (std::list<Triple>::iterator J = merged_list_cols.begin();
                 J != merged_list_cols.end(); J++) {
                std::string unknown;
                if(colnum>=merged_list_supp_cols.size())
                    unknown = main_unknown;
                else
                    unknown = "[conflict]";
                colnum++;

                current_col_2 = (*J).GetIndex_2();
                value = take_value_from_line(current_line_2, current_col_2,
                                             OffsetSuppCols_2, nb_supp_elements,
                                             value_length, separator, unknown);
                if (last_iteration == true && value == "0/1") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }
                if (last_iteration == true && value == "2/2") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }

                if (last_iteration == true && value == "1/0") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }
                J++;
                if (J != merged_list_cols.end())
                    output_file << value << separator;
                else
                    output_file << value << '\n';
                J--;
            }

        } else { // if (current_row_1 != -1 && current_row_2 != -1) {
            current_line_1 =
                GetLine(VCFFile1, OfsFile1, current_row_1, nb_supp_lines1);
            current_line_2 =
                GetLine(VCFFile2, OfsFile2, current_row_2, nb_supp_lines2);

            unsigned int colnum=0;
            for (std::list<Triple>::iterator J = merged_list_cols.begin();
                 J != merged_list_cols.end(); J++) {
                std::string unknown;
                if(colnum>=merged_list_supp_cols.size())
                    unknown = main_unknown;
                else
                    unknown = "[conflict]";
                colnum++;

                current_col_1 = (*J).GetIndex_1();
                current_col_2 = (*J).GetIndex_2();
                if (current_col_1 != -1 && current_col_2 == -1) {
                    value = take_value_from_line(
                        current_line_1, current_col_1, OffsetSuppCols_1,
                        nb_supp_elements, value_length, separator, unknown);

                } else if (current_col_1 == -1 && current_col_2 != -1) {
                    value = take_value_from_line(
                        current_line_2, current_col_2, OffsetSuppCols_2,
                        nb_supp_elements, value_length, separator, unknown);

                } else {
                    value_1 = take_value_from_line(
                        current_line_1, current_col_1, OffsetSuppCols_1,
                        nb_supp_elements, value_length, separator, unknown);

                    value_2 = take_value_from_line(
                        current_line_2, current_col_2, OffsetSuppCols_2,
                        nb_supp_elements, value_length, separator, unknown);

                    if (value_1 == value_2)
                        value = value_1;
                    else if (value_1 == unknown && value_2 != unknown)
                        value = value_2;
                    else if (value_1 != unknown && value_2 == unknown)
                        value = value_1;
                    else if (value_1 != unknown && value_2 != unknown &&
                             value_1 != value_2)
                        value = "2/2";
                }
                if (last_iteration == true && value == "0/1") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }
                if (last_iteration == true && value == "2/2") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }

                if (last_iteration == true && value == "1/0") {
                    incoherence_file << (*I).GetName() << '\t' << (*J).GetName()
                                     << std::endl;
                    value = unknown;
                }
                J++;
                if (J != merged_list_cols.end())
                    output_file << value << separator;
                else
                    output_file << value << '\n';
                J--;
            }
        }
    }
    output_file.close();
    incoherence_file.close();
    VCFFile1.close();
    VCFFile2.close();
    OfsFile1.close();
    OfsFile2.close();
    std::remove(OfsFileName1.c_str());
    std::remove(OfsFileName2.c_str());

    log_file << "     - Number of colnames: " << merged_list_cols.size() << "."
             << std::endl;
    log_file << "     - Number of rownames: " << merged_list_rows.size() << "."
             << std::endl;
}

// Take value from line using the position of corresponding colname
std::string take_value_from_line(std::string line, int current_col,
                                 int &OffsetSuppCols, int nb_supp_elements,
                                 int value_length, char separator,
                                 std::string unknown) {
    int line_length = line.length(), begin_colposition = 0,
        begin_next_colposition = 0;
    if (OffsetSuppCols == -1) {
        for (int j = 0; j < nb_supp_elements; j++) {
            begin_colposition = begin_next_colposition;
            begin_next_colposition = GetNextColnamePosition(
                line, begin_colposition, line_length, separator);
            OffsetSuppCols = begin_next_colposition;
        }
    }
    std::string value;
    begin_colposition = 0;
    begin_next_colposition = 0;
    if (current_col == -1)
        value = unknown;
    else if (current_col < nb_supp_elements) {
        for (int j = 0; j <= current_col; j++) {
            begin_colposition = begin_next_colposition;
            begin_next_colposition = GetNextColnamePosition(
                line, begin_colposition, line_length, separator);
        }
        return line.substr(begin_colposition,
                           begin_next_colposition - begin_colposition - 1);
    } else {
        value = line.substr(OffsetSuppCols + (current_col - nb_supp_elements) *
                                                 (value_length + 1),
                            value_length);
    }
    return value;
}

// Create auxiliary file which contains the position of the beginning of each
// line in the file in binary format
std::string CreateOffsetsFile(std::string filename,
                              std::string file_extension) {
    std::string InputFileName =
        filename.substr(0, filename.length() - file_extension.length() - 1);
    std::ifstream vcf_file(filename.c_str(),
                           std::ifstream::in | std::ifstream::binary);
    assert(vcf_file.is_open());
    std::string ofs_filename = InputFileName + ".ofs";
    std::ofstream ofs_file(ofs_filename.c_str(),
                           std::ofstream::out | std::ofstream::binary);
    assert(ofs_file.is_open());

    std::string CurrentLine;
    int CurrentOffset = 0;
    ofs_file.write((char *)&CurrentOffset, sizeof(CurrentOffset));

    while (!vcf_file.eof()) {
        getline(vcf_file, CurrentLine);
        if (CurrentLine.length() > 0) {
            CurrentOffset += CurrentLine.length() + 1;
            ofs_file.write((char *)&CurrentOffset, sizeof(CurrentOffset));
        }
    }
    vcf_file.close();
    ofs_file.close();

    return ofs_filename;
}

// Get a line from file using corresponding position of line in the file
std::string GetLine(std::ifstream &vcf_file, std::ifstream &ofs_file,
                    int NumLine, int nb_supp_lines) {
    ofs_file.seekg((NumLine + nb_supp_lines) * sizeof(int), std::ios::beg);
    int CurrentOffset;
    ofs_file.read((char *)&CurrentOffset, sizeof(CurrentOffset));
    vcf_file.seekg(CurrentOffset, std::ios::beg);
    std::string ResultLine;
    getline(vcf_file, ResultLine);
    return ResultLine;
}

// Apply all functions above for merging two files
void process_data(std::string liststring, std::string file_name_1,
                  std::string file_name_2, std::string output_path,
                  std::string output_filename, std::string unknown,
                  char comment_sign, char separator, bool IsSorted,
                  int nb_supp_elements, int value_length,
                  std::string file_extension, int nb_supp_lines,
                  bool last_iteration, int num_col_sort) {
    std::list<std::string> list_rownames_1, list_rownames_2,
        list_colnames_supp_1, list_colnames_main_1, list_colnames_supp_2,
        list_colnames_main_2;
    Couple *list_indexed_rows_1, *list_indexed_rows_2, *list_indexed_cols_1,
        *list_indexed_cols_2;
    std::list<Triple> general_list_rownames, main_list_colnames, supp_list_colnames;

    std::ofstream log_file((output_path + "log.txt").c_str(),
                           std::ios_base::app);
    assert(log_file.is_open());

    log_file << "Merge:" << std::endl;
    log_file << " - Input:" << std::endl;
    log_file << "   - " << file_name_1 << std::endl;

    make_list_of_colnames(file_name_1, output_path, log_file,
                          list_colnames_supp_1, list_colnames_main_1,
                          comment_sign, nb_supp_elements, separator);
    list_rownames_1 =
        make_list_of_rownames(file_name_1, output_path, log_file, comment_sign,
                              separator, num_col_sort);
    log_duplicated_rowname(log_file, list_rownames_1);

    log_file << "   - " << file_name_2 << std::endl;
    make_list_of_colnames(file_name_2, output_path, log_file,
                          list_colnames_supp_2, list_colnames_main_2,
                          comment_sign, nb_supp_elements, separator);
    list_rownames_2 =
        make_list_of_rownames(file_name_2, output_path, log_file, comment_sign,
                              separator, num_col_sort);
    log_duplicated_rowname(log_file, list_rownames_2);

    list_indexed_rows_1 = enumerate_list(list_rownames_1, {}, IsSorted);
    list_indexed_rows_2 = enumerate_list(list_rownames_2, {}, IsSorted);

    list_indexed_cols_1 =
        enumerate_list(list_colnames_main_1, list_colnames_supp_1, IsSorted);
    list_indexed_cols_2 =
        enumerate_list(list_colnames_main_2, list_colnames_supp_2, IsSorted);

    general_list_rownames =
        merge_list(list_indexed_rows_1, list_indexed_rows_2,
                   list_rownames_1.size(), list_rownames_2.size());
    
    main_list_colnames =
        merge_list(list_indexed_cols_1+list_colnames_supp_1.size(), list_indexed_cols_2+list_colnames_supp_1.size(),
                   list_colnames_main_1.size(),
                   list_colnames_main_2.size());
    
    supp_list_colnames =
        merge_list(list_indexed_cols_1, list_indexed_cols_2,
                   list_colnames_supp_1.size(),
                   list_colnames_supp_2.size());

    delete[] list_indexed_rows_1;
    delete[] list_indexed_rows_2;
    delete[] list_indexed_cols_1;
    delete[] list_indexed_cols_2;

    log_file << " - Output:" << std::endl;
    log_file << "   - " << output_filename << std::endl;

    fusion(liststring, file_name_1, file_name_2, output_path, output_filename,
           log_file, general_list_rownames, supp_list_colnames, main_list_colnames, comment_sign,
           unknown, separator, nb_supp_elements, value_length, file_extension,
           nb_supp_lines, last_iteration);
    log_file << std::endl;
    log_file.close();
}
