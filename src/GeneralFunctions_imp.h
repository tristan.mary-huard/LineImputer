#ifndef GENERALFUNCTIONS_H
#define GENERALFUNCTIONS_H

#include "Constants.h"
#include <assert.h>
#include <fstream>
#include <iostream>

// In the next function:
// n is an integer containing 0 1 or 2 digits where the 2 have been replaced
// with 0. Positions is the array of the positions of the digits 2 that have
// been replaces by 2. NbPositions is the number of positions. Value is an
// integer 0 <= Value < (1 << NbPositions) It contains the digits that should be
// written in n For instance if inout data is: n = 01001010 01010010 10100101
// 01000101 P = ...X...X X...XX.. .X....X. X.....X. (NbPositions is 9) and
// Values = 00000000 00000000 00000001 10101011
//
// Then the returned result should be:
//
//          n = 01001010 01010010 10100101 01000101
//          P = ...X...X X...XX.. .X....X. X.....X.
// and Values = 00000000 00000000 00000001 10101011
//          P = ...1...1 0...10.. .1....0. 1.....1.
//     Answer = 01011011 01011010 11100101 11000111

unsigned long int GetFileSize(std::ifstream &File);

void AddTabs(std::string FileName);

unsigned int Fill(unsigned int n, unsigned char *Positions, unsigned int Values,
                  unsigned int NbPositions);

unsigned int RightShift(unsigned int LetterAtLeft, unsigned int RightDigit,
                        unsigned short int MarkovChainOrder);

unsigned int LeftShift(unsigned int LetterAtRight, unsigned int LeftDigit,
                       unsigned short int MarkovChainOrder);

unsigned int GetInitial(char *Ind, unsigned char *PosOf2, unsigned int &Nb2,
                        unsigned short int LengthInd);

long double ProbaToBe1LittleEndian(long double *Tau,
                                   unsigned short int MarkovChainOrder);

long double ProbaToBe1BigEndian(long double *Tau,
                                unsigned short int MarkovChainOrder);

int CompatibleLetters(unsigned int Basis2, unsigned int Basis3,
                      unsigned short int MarkovChainOrder);

void FromVCFToMatrix(std::string InputFileName, std::string OutptFileName,
                     unsigned int &NbInd, unsigned int &NbMarkers);

int Prec1(char c);
int Prec2(char c);
int Next1(char c);
int Next2(char c);

// function that gives the length of a line
unsigned int LineLength(std::ifstream &InputFile, unsigned int &FileSize);

int get_end_metadata_position(std::string Line, int line_length,
                              int nb_supp_elements);

int get_next_value_position(std::string Line, int FirstPos, int line_length);

void ImpThreshold(std::string probas_filename, std::string output_filename,
                  double threshold, int nb_supp_elements, int ncols,
                  bool replace_values, bool do_threshold);

void txt_to_vcf(std::string txt_filename, std::string vcf_filename,
                std::string output_filename, int nb_supp_elements, int nb_cols,
                bool replace_values);

template <typename T>
void FromBinToHR(std::string BinFileName, std::string HRFileName,
                 unsigned int LineL) {
    unsigned int FileSize, NbLines;
    std::ifstream BinFile(BinFileName.c_str(),
                          std::ifstream::in | std::ifstream::binary);
    BinFile.seekg(0, std::ios::end);
    FileSize = (unsigned int)BinFile.tellg();
    BinFile.seekg(0, std::ios::beg);

    assert(BinFile.is_open());
    std::ofstream HRFile(HRFileName.c_str(),
                         std::ofstream::out | std::ofstream::binary);
    assert(HRFile.is_open());

    assert(FileSize % LineL == 0);
    NbLines = FileSize / LineL;
    assert((LineL - 1) % sizeof(T) == 0);
    unsigned int NbOutCols = (LineL - 1) / sizeof(T);
    T *Buffer = new T[LineL];
    char c;
    for (unsigned int i = 0; i < NbLines; i++) {
        BinFile.read((char *)Buffer, NbOutCols * sizeof(T));
        BinFile.read(&c, 1);
        for (unsigned int j = 0; j < NbOutCols; j++) {
            T Aux = Buffer[j];
            HRFile << Aux;
            if (j < NbOutCols - 1)
                HRFile << '\t';
            else
                HRFile << '\n';
        }
    }
    delete[] Buffer;
    HRFile.close();
    BinFile.close();
}

// Boolean false if we have other than 1 or 2 or 3 or 4 (ex: NA or .) at
// position t and t+1
bool BothKnown(char *A);
// Boolean false if we have other than 1 or 2 or 3 or 4 (ex: NA or .) at
// position t
bool Known(char A);

double P(char Y, char X);

int MaxOf4(double *);

int FirstLetter(int);
int LastLetter(int);
char FirstLetterChar(char i);
char LastLetterChar(char i);

void From4To2Letters(char *BufferOn4Letters, char *BufferOn2Letters,
                     int Buffer4Size);

void CheckFile(std::ifstream &File, std::string OutputFileName);

bool CheckBuffer(char *Buffer, int First, int last, int &ErrIndex);

void Transpose(std::string InputFileName, std::string OutputFileName,
               unsigned int ElementSize = 1, unsigned int LineL = 0);

int RemoveTabs(char *Buffer, int BufferSize);

void Normalize(double *P);

void EraseProbas(double *P, const char &c);

#endif // GENERALFUNCTIONS_H
